import { memo } from "react"
import { Link } from "react-router-dom"
import Item from "../Item/Item"


// memo (componente) -> memo (componente, funcionComparacion)

const ItemList = memo( ( { products } ) => {
      console.log('itemList')
      return (
        products.map( product =>  <Item key={product.id} product={product}  />)
      )
    }
)

export default ItemList

// [] = products.filter( product => product.id === '1' )
// {} = products.find( product => product.id === '1')