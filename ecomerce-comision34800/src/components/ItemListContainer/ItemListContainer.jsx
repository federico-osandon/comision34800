import { useEffect, useState } from 'react'
import { Link, useParams } from 'react-router-dom'
import { gFetch } from '../../utils/gFetch'
import ItemList from '../ItemList'

import { getFirestore, doc, getDoc, collection, getDocs, query, where, limit, orderBy } from 'firebase/firestore'


const ItemListContainer = (obj) => {  
    const [products, setProducts] = useState([])
    
    const [product, setProduct] = useState({})

    const [loading, setLoading] = useState(true)
    const {categoriaId} = useParams()
    const [ bool, setBool] = useState(false)

// traer un producto por id de firestore
    // useEffect(() => {
    //     const dbFirestore = getFirestore()
    //     const queryCollection = doc(dbFirestore, 'items', 'w3oyaDFZwnQgnZZ93DAU')
    //     getDoc(queryCollection)
    //     .then((doc) => setProduct(   { id: doc.id, ...doc.data() }  ))
    // },[])

    // traer todos los productos de firestore
    // useEffect(() => {
    //     const dbFirestore = getFirestore()
    //     const queryCollection = collection(dbFirestore, 'items')
    //     getDocs(queryCollection)
    //     .then((resp) => setProducts( resp.docs.map(doc => ( { id: doc.id, ...doc.data() } ) ) ))
    //     .catch(err => console.log(err))
    //         .finally(()=>setLoading(false)) 
    //     .then((doc) => setProduct(   { id: doc.id, ...doc.data() }  ))
    // },[])

    // todos los productos por filtro de categoria
    useEffect(() => {

        const dbFirestore = getFirestore()
        const queryCollection = collection(dbFirestore, 'items')

        let queryFilter = query(queryCollection, where('price', '>=', 1500), limit(2), orderBy('price', 'asc') )  
        // let queryFilter = query(queryCollection, where('categoria', '==', 'remeras'), limit() )  

        getDocs(queryFilter)
        .then((resp) => setProducts( resp.docs.map(doc => ( { id: doc.id, ...doc.data() } ) ) ))
        .catch(err => console.log(err))
        .finally(()=>setLoading(false)) 
        .then((doc) => setProduct(   { id: doc.id, ...doc.data() }  ))
    },[])
        
    // useEffect(()=> {
    //     const dbFirestore = getFirestore()
    //     const queryCollection = collection(dbFirestore, 'items')
    //     if (categoriaId) {
    //         let queryFilter = query(queryCollection, where('categoria', '==', categoriaId) )  

    //         getDocs(queryFilter)
    //         .then((resp) => setProducts( resp.docs.map(doc => ( { id: doc.id, ...doc.data() } ) ) ))
    //         .catch(err => console.log(err))
    //         .finally(()=>setLoading(false)) 
    //         .then((doc) => setProduct(   { id: doc.id, ...doc.data() }  ))
            
    //     }else{
    //         getDocs(queryCollection)
    //         .then((resp) => setProducts( resp.docs.map(doc => ( { id: doc.id, ...doc.data() } ) ) ))
    //         .catch(err => console.log(err))
    //             .finally(()=>setLoading(false)) 
    //         .then((doc) => setProduct(   { id: doc.id, ...doc.data() }  ))
    //     }       
    // }, [categoriaId])
    
    // .then(respuesta => console.log(respuesta))

    const agregarProducto = () => {
        setProducts([...products, {id: products.length, name: `Producto ${products.length}`}])
    }

    const cambiarEstado = () => {
        setBool(!bool)
    }
    // console.log(categoriaId)
    
    // [1,2,3] => [<li>1</li>, <li>2</li>,<li>3</li>]
   
    console.log(products)
    return (
        loading 
            ? 
                <h2>CArgando...</h2>            
            :
                <div>
                    <h1>ItemListContainer</h1>  
                    {/* <button onClick={cambiarEstado}>cambiar estado</button>    */}
                    
                    <ItemList products={products} />
                    <button onClick={cambiarEstado}>Me gusta</button>
                    <button onClick={agregarProducto}>Agregar Productos</button>
                </div>
    )
}
// los eventos me disparan una nueva ejecución del componente donde se esta ejecutando 

export default ItemListContainer