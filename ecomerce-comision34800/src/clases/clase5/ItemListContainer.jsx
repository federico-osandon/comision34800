import { useEffect, useState } from 'react'

const ItemListContainer = (obj) => {
    const [contador, guardarEnContador] = useState(0)
    const [ bool, setBool ] = useState(true)
    

    const sumar = () => {
        //  count ++ // count = coutn + 1 - count += 1
        //  console.log(count)
        guardarEnContador(contador + 1) //-> 0 + 1 - 1 +  1 
    }
    const cambiarEstado= () => {
        setBool(!bool)
    }
    
    useEffect(()=>{
        console.log('se ejecuta siempre por cada render - 1') // ej fetch llamada a un api 1
        // addEventListener()
        // console.log('add event listener')
        // return ()=>{
        //     console.log('limieza - desmontado - removeEventListener')
        // }
    })


    useEffect(()=>{
        console.log('tareas pesadas sincrónicas y asincrónico - api - 2') // ej fetch llamada a un api 1
        // addEventListener()
        // console.log('add event listener')
        // return ()=>{
        //     console.log('limieza - desmontado - removeEventListener')
        // }
    }, [])


    useEffect(()=>{
        console.log('cambio de bool 3') // ej fetch llamada a un api 1
        // addEventListener()
        // console.log('add event listener')
        // return ()=>{
        //     console.log('limieza - desmontado - removeEventListener')
        // }
    }, [bool, contador])
    

    
    console.log('ItemListContainer - 4')
    // alert('bloqueando el rendering')  // dimulando tarea pesadas

    return (
        <div>
            {/* {obj.greeting} */}
            {/* { obj.children } */}
            { contador }
            <button onClick={sumar} > + </button> 
            <button onClick={cambiarEstado} > cambiar estado </button> 
        </div>
    //     guardarEn
    )
}
// los eventos me disparan una nueva ejecución del componente donde se esta ejecutando 

export default ItemListContainer