import { useState } from 'react'
import reactLogo from './assets/react.svg'
import './App.css'
import Menu from './components/Menu/Menu'


function Titulo ( { titulo, subtitulo }) {
    // console.log(props)
    // const {titulo, subtitulo} = props
     
    return( 
        <div>
            <h2>{titulo}</h2>   
            <h3>{subtitulo}</h3>
        </div>
    )
    
}
const Form = ({tituloForm}) => {

    //variable de estado
    return  <form>
                <Titulo 
                    titulo={tituloForm}
                    // subtitulo={subtituloApp}
                />
                <input type="text" placeholder='ingrese nombre' />
                <br />
                <input type="text" placeholder='ingrese apellido' />
            </form>
}

const Section = ({children}) => {
    // console.log(props)
    return  <section>
                {children}
                <h2>Titulo de section</h2>
                <p>Parrafo de section</p>
            </section>
}


// <Tiutlo titulo='' subtitulo='' /> es un componente equivalente Titulo({titulo, subtitulo})

function App() {
    let tituloApp = 'Hola soy titulo de app'
    let subtituloApp = 'Hola soy subtitulo de app'
    let tituloForm = 'Hola soy titulo de form'

    // estan las llamadas a las api
    return (
        <div>
            <Menu />
            <Form tituloForm={tituloForm} />    
            
            <Section >
                <Titulo titulo='titulo de App' subtitulo='sub de app' />
            </Section>
            

        </div>
    )
}
export default App





